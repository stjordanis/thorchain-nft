const fs = require('fs');

let dateData = [];
let stakeData = [];

const loadData = async () => {
    let raw = fs.readFileSync('./dateAllocations-final.json', 'utf8')
    dateData = JSON.parse(raw);
    let raw2 = fs.readFileSync('./stakeAllocations-final.json', 'utf8')
    stakeData = JSON.parse(raw2);
}

const distroTokens = async () => {
    for (const token of dateData) {
        await distroToken(token)
    }
    for (const token of stakeData) {
        await distroToken(token)
    }
}

const distroToken = async (token) => {
    console.log(`Distributing token ID ${token.tokenID} to ${token.members} members`) 
    // distro token with EthersJS
}


const main = async () => {
    await loadData();
    distroTokens()
}

main()
