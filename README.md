# THORChain Collectibles - Journey to Asgard

This repo handles:

* Scrape for elligible members from address `bnb1z7kmwvvldnq2s2lxtwhcq7h5qjekpj6dr680ue`
* Load up 12 months of RUNEVault staking data, sort into weeks
* Allocate collectibles based on time
* Allocate collectibles based on stake
* Mint all the collectibles
* Distro the collectibles out

## Mint

```
cd mint
node mint.js
```

Will simulate minting out all collectibles, adding in the required metadata. 


## Distro

```
cd distro
node distro.js
```

Will simulate distributing out all collectibles in batches to all recipients. 